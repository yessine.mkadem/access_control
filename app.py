# -*- coding: utf-8 -*-

import json
import mysql.connector
from datetime import datetime

from flask import Flask, render_template, request, jsonify, redirect
import os

app = Flask(__name__)
app.static_folder = 'static'


# using this configuration in the app.config the page will reload every time
@app.route("/")
def home():
    return render_template("main.html")


# create a route for entering new question and response
@app.route("/nouveau")
def nouveau():
    return render_template("new.html")


@app.route("/submit_data", methods=['POST'])
def submit_data():
    # get the name and rfid values from the HTML form
    name = request.form['name']
    rfid = request.form['rfid']
    date_affectation = datetime.now()
    date_affectation = date_affectation.strftime("%d/%m/%Y %H:%M:%S")
    # create a connection to MySQL
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()

    # execute an INSERT query to insert the new record into the users table
    query = "INSERT INTO users (name, rfid,date_affectation) VALUES (%s, %s,%s)"
    values = (name, rfid,date_affectation)
    cursor.execute(query, values)
    # execute an insert query to the apidata the new record into the apidata table
    query2 = "INSERT INTO apidata (name, rfid) VALUES (%s, %s)"
    values2 = (name, rfid)
    cursor.execute(query2, values2)
    # commit the transaction
    cnx.commit()

    # close the cursor and the database connection
    cursor.close()
    cnx.close()


    return render_template("main.html")


@app.route("/utilisateurs")
def utilisateurs():
    # create a connection to MySQL
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()

    # execute a SELECT query to retrieve all records from the users table
    #query = "SELECT * FROM users"
    query = "SELECT * FROM users WHERE date_fin IS NULL OR date_fin = ''"
    cursor.execute(query)

    # fetch all records and store them in a variable
    users = cursor.fetchall()
    # close the cursor and the database connection
    cursor.close()
    cnx.close()

    return render_template("utilisateur.html",users=users)


@app.route("/delete_record", methods=["POST"])
def delete_record():
    # create a connection to MySQL
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()

    # get the id of the user to be deleted from the form data
    id = request.form['id']

    # execute a DELETE query to delete the user with the given id
    query = "DELETE FROM users WHERE id = %s"
    cursor.execute(query, (id,))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    query = "SELECT * FROM users"
    cursor.execute(query)
    users = cursor.fetchall()
    cursor.close()
    cnx.close()
    return render_template("utilisateur.html",users=users)


@app.route("/edit", methods=["GET", "POST"])
def edit():
    # create a connection to MySQL
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()

    # retrieve the record to be edited using the id value
    id = request.args.get('id')
    query = "SELECT * FROM users WHERE id = %s"
    cursor.execute(query, (id,))
    user = cursor.fetchone()

    if request.method == 'POST':
        # get the updated values from the form
        id = request.form['id']
        name = request.form['name']
        rfid = request.form['rfid']

        # execute an UPDATE query to update the record in the MySQL table
        query = "UPDATE users SET name = %s, rfid = %s WHERE id = %s"
        cursor.execute(query, (name, rfid, id))

        # commit the transaction and close the cursor and connection
        cnx.commit()
        cursor.close()
        cnx.close()

        # redirect the user to the homepage
        return redirect('/utilisateurs')

    # close the cursor and connection
    cursor.close()
    cnx.close()
    # render the HTML template with the record to be edited
    return render_template('edit.html', user=user)

@app.route("/deactivate",methods=["POST"])
def deactivate():
    # create a connection to MySQL
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()
    # get the id of the user to be deleted from the form data
    id = request.form['id']
    # define the end date in which the admin end the end the user
    date_affectation = datetime.now()
    date_affectation = date_affectation.strftime("%d/%m/%Y %H:%M:%S")

    # execute a DELETE query to delete the user with the given id
    query = "UPDATE users SET date_fin = %s WHERE id = %s"
    cursor.execute(query, (date_affectation,id))

    # commit the transaction and close the cursor and connection
    cnx.commit()
    query = "SELECT * FROM users WHERE date_fin =''"
    cursor.execute(query)
    users = cursor.fetchall()
    cursor.close()
    cnx.close()
    return render_template("utilisateur.html",users=users)


@app.route("/api/userdata",methods=["GET"])
def userdata():
    # create a connection to mysql database
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )
    # create a cursor
    cursor = cnx.cursor()

    # execute a select query to find all the content of new user stored in api data
    query = "SELECT * FROM apidata"
    cursor.execute(query)
    data = cursor.fetchall()
    rfidcode = []
    for row in data:
        rfidcode.append(row[2])


    # execute a delete query to delete all the content stored in api data
    query = "DELETE FROM apidata"
    cursor.execute(query)
    # commit the transaction and close the cursor and connection
    cnx.commit()
    # close the cursor and connection
    cursor.close()
    cnx.close()

    # return the data as a JSON response
    return rfidcode

@app.route("/api/allusers",methods=["GET"])
def allusers():
    # create a connection to mysql database
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )
    # create a cursor
    cursor = cnx.cursor()

    # execute a select query to find all the content of new user stored in api data
    query = "SELECT * FROM users WHERE date_fin =''"
    cursor.execute(query)
    data = cursor.fetchall()
    rfidcode = []
    for row in data:
        rfidcode.append(row[2])

    # close the cursor and connection
    cursor.close()
    cnx.close()

    # return the data as a JSON response
    return rfidcode

@app.route("/api/<string:rfid>/<string:direction>", methods=["GET"])
def mouvement(rfid, direction):
    # create a connection to MySQL database
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )

    # create a cursor
    cursor = cnx.cursor()
    # find the datime of the request
    date = datetime.now()
    date = date.strftime("%d/%m/%Y %H:%M:%S")
    # search for the user with this rfid
    query = "SELECT id FROM users WHERE rfid=%s"
    cursor.execute(query,(rfid,))
    data = cursor.fetchone()

    # extract the user id from the tuple
    user_id = data[0]
    print(user_id)
    # execute an INSERT query to insert the new record into the mouvement table
    query = "INSERT INTO mouvement (user_id,datetime, move_sens) VALUES (%s, %s, %s)"
    values = (user_id, date,direction)
    cursor.execute(query, values)

    # commit the transaction
    cnx.commit()

    # close the cursor and connection
    cursor.close()
    cnx.close()

    return 'New movement added'

@app.route("/mouvement",endpoint="mouvement.html")
def mouvement():
    # create a connection to MySQL database
    cnx = mysql.connector.connect(
        user='root',
        password='',
        host='localhost',
        database='usersid',
        auth_plugin='mysql_native_password'
    )
    # extract the rfid code from the request
    id = request.args.get('id')
    # create a cursor
    cursor = cnx.cursor()
    # execute a SELECT query to get data from the mouvement and users tables
    query = "SELECT u.name, m.datetime, m.move_sens FROM mouvement m JOIN users u ON m.user_id = u.id" \
            " WHERE u.id = %s  ORDER BY m.datetime DESC"
    # do not forget to get the user id then display in the table user_name and user_rfid
    # save the operation and then send them to the database in your system
    # another thing is to not forget the offline solution in arduino ide
    cursor.execute(query, (id,))

    data = cursor.fetchall()

    # close the cursor and connection
    cursor.close()
    cnx.close()

    # render the template with the data
    return render_template("mouvement.html", data=data)


if __name__ == "__main__":
    app.run(debug=True, port=5001, host='0.0.0.0')