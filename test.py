import json
import random

# Define the size of the array
array_size = 3500

# Define the length of each string in the array
string_length = 10

# Generate an array of random hexadecimal strings
card = []
for i in range(array_size):
    hex_string = "".join([random.choice("0123456789ABCDEF") for j in range(string_length)])
    card.append(hex_string)

# Reshape the array into a list of lists, each containing 10 values
card_reshape = [card[i:i+10] for i in range(0, len(card), 10)]
# Save the array to a JSON file
data = {"card": card}
with open("card_array.json", "w") as f:
    json.dump(data, f,indent=4)
